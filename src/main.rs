use std::io;
//use crossterm::style::Styler;

const LITERS_PER_METER: f32 = 4.0;

fn main() {
    let mut gardens = Vec::new();
    let mut i = 1;

    loop {
        println!("\nWhat is the width of your {}{} garden?", i, get_int_suffix(i));
        let garden_width = get_input();
        println!("\nWhat is the length of your {}{} garden?", i, get_int_suffix(i));
        let garden_length = get_input();

        gardens.push(garden_width);
        gardens.push(garden_length);

        println!("\nWould you like to add another garden?");;
        if get_confirmation() {
            i += 1;
            continue;
        }
        break;
    }

    let mut total_area = 0.0;
    let number_of_gardens = gardens.len() / 2;

    for i in 0..number_of_gardens {
        total_area += gardens[i * 2] * gardens[i * 2 + 1];
    }

    let total_water = total_area * LITERS_PER_METER;
    println!("\nFor a combined garden area of {} m^2, you will need {} liters of water per day.", total_area, total_water);
}

fn get_input() -> f32 {
    loop {
        let mut input = String::new();

        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");
        
        let input: f32 = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Not a valid input");
                continue;
            },
        };

        return input;
    }
}

fn get_confirmation() -> bool {
    loop {
        let mut string = String::new();

        io::stdin()
            .read_line(&mut string)
            .expect("Failed to read line.");

        let char_vec: Vec<char> = string.chars().collect();

        if char_vec[0] == 'y' {
            return true;
        }
        if char_vec[0] == 'n' {
            return false;
        }
        else {
            println!("'{}' is not a valid input, try 'y' or 'n'.", char_vec[0]);
        }
    }
}

fn get_int_suffix(number: usize) -> &'static str {
    match number % 10 {
        1 => return "st",
        2 => return "nd",
        3 => return "rd",
        _ => return "th"
    };
}
